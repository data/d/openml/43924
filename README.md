# OpenML dataset: eucalyptus

https://www.openml.org/d/43924

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Find out which seedlots in a species are best for soil conservation in dry hill country. Bulluch B. T., (1992) Eucalyptus Species Selection for Soil Conservation in Seasonally Dry Hill Country - Twelfth Year Assessment New Zealand Journal of Forestry Science 21(1): 10 - 31 (1991)

Kirsten Thomson and Robert J. McQueen (1996) Machine Learning Applied to Fourteen Agricultural Datasets. University of Waikato Research Report
https://www.cs.waikato.ac.nz/ml/publications/1996/Thomson-McQueen-96.pdf + the original publication:

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43924) of an [OpenML dataset](https://www.openml.org/d/43924). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43924/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43924/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43924/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

